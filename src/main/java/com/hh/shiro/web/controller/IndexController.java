package com.hh.shiro.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/main")
public class IndexController {
	
   @RequestMapping(value = "/index")
   public ModelAndView itsIndex() {
      ModelAndView mv = new ModelAndView("auth/index");
      return mv;
   }

}
