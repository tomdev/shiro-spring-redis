package com.hh.shiro.cache;

import org.apache.shiro.cache.Cache;

public interface IShiroCacheManager {

    <K, V> Cache<K, V> getCache(String name);

    void destroy();

}
